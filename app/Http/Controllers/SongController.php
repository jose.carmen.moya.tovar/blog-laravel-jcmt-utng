<?php

namespace App\Http\Controllers;

use App\Models\song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $songs = song::all();
        return view('song.index', compact('songs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('song.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $song = new song();
        $song->song = $request->input('playSong');
        $song->artist = $request->input('playArtist');
        $song->album = $request->input('playAlbum');
        if($song->save()){
            return redirect()->back()->with('success', 'Saved succesfully!');
        }
        return redirect()->back()->with('success', 'Could not save!');
    }

    /**
     * Display the specified resource.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function show(song $song)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $song = song::find($id);
        return view("song.edit", compact('song'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $song = song::find($id);
        $song->song = $request->input('playSong');
        $song->artist = $request->input('playArtist');
        $song->album = $request->input('playAlbum');
        if($song->save()){
            return redirect()->back()->with('success', 'Updated succesfully!');
        }
        return redirect()->back()->with('success', 'Could not update!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\song  $song
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        if(song::destroy($id)){
            return redirect()->back()->with('deleted', 'Deleted succesfully!');
        }
        return redirect()->back()->with('delete-failed', 'Could not delete!');
    }
}
