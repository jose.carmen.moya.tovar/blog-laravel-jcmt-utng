@extends('layout')
@section('dashboard-content')
    <h1>Edit Playlist</h1>

    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="gone">
            <strong>{{ (Session::get('success')) }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden=true>&times;</span>
            </button>
        </div>
    @endif

    @if (Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong>{{ (Session::get('failed')) }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden=true>&times;</span>
            </button>
        </div>
    @endif


    <form action="{{ URL::to('update-song-form')}}/{{$song->id}}" method="get">
        @csrf
        <div class="form-group">
            <label for="playSong">Song</label>
            <input value="{{ $song->song }}" type="text" class="form-control mt-3" id="playSong" name="playSong"aria-describedby="emailHelp" placeholder="Enter Song">
        </div>
        <div class="form-group">
            <label for="playArtist">Artist</label>
            <input value="{{ $song->artist }}" type="text" class="form-control mt-3" id="playArtist" name="playArtist"aria-describedby="emailHelp" placeholder="Enter Artist">
        </div>
        <div class="form-group">
            <label for="playAlbum">Album</label>
            <input value="{{ $song->album }}" type="text" class="form-control mt-3" id="playAlbum" name="playAlbum"aria-describedby="emailHelp" placeholder="Enter Album">
        </div>
        
        <button type="submit" class="btn btn-primary mt-3">Update</button>
    </form>  
@stop