@extends('layout')
@section('dashboard-content')
    <h1>Add a Playlist</h1>

    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert" id="gone">
            <strong>{{ (Session::get('success')) }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden=true>&times;</span>
            </button>
        </div>
    @endif

    @if (Session::get('failed'))
        <div class="alert alert-warning alert-dismissible fade show" role="alert" id="gone">
            <strong>{{ (Session::get('failed')) }}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden=true>&times;</span>
            </button>
        </div>
    @endif

    <form action="{{ URL::to('post-song-form')}}" method="post">
        @csrf
        <div class="form-group">
            <label for="playSong">Song</label>
            <input type="text" class="form-control mt-3" id="playSong" name="playSong"aria-describedby="emailHelp" placeholder="Enter Song">
        </div>
        <div class="form-group">
            <label for="playArtista">Artist</label>
            <input type="text" class="form-control mt-3" id="playArtist" name="playArtist"aria-describedby="emailHelp" placeholder="Enter artist">
        </div>
        <div class="form-group">
            <label for="playAlbum">Album</label>
            <input type="text" class="form-control mt-3" id="playAlbum" name="playAlbum"aria-describedby="emailHelp" placeholder="Enter album">
        </div>
        
        <button type="submit" class="btn btn-primary mt-3">Add</button>
    </form>
@stop